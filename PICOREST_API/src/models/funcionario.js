'use strict'
const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate')

const FuncionarioSchema = new mongoose.Schema({
    matricula: {
        type: String,
        unique: true,
        require: true
    },
    dataAdmissao: {
        type: Date
    }, 
    ctps: {
        type: String
    },
    rg: {
        type: String
    },
    cnh: {
        type: String
    },
    endereco: {
        numero: {
            type: String
        },
        rua: {
            type: String
        }, 
        bairro: {
            type: String
        },
        cep: {
            type: String
        },
        cidade: {
            type: String
        }, 
        uf: {
            type: String
        },
        email: {
            type: String
        }
    
    },
    cursos: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Curso'
    }],
    setor: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Setor'
    },
    funcao: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Funcao'
    },
    usuario: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Usuario'
    },
    createAt: {
        type: Date,
        default: Date.now
    }
})

FuncionarioSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('Funcionario', FuncionarioSchema)

