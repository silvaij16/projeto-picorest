'use strict'
const mongoose = require('mongoose')
// const mongoose = require('../database/banco')
// const restful = require('node-restful')
// const mongoose = restful.mongoose
//const mongoosePaginate = require('mongoose-paginate')

const CursoSchema = new mongoose.Schema({
    descricao: { 
        type: String, 
        required: true
    }, 
    cargaHoraria: {
        type: String,
        required: true
    },
    certificado: { 
        type: String,
        required: true
    },
    dataInicio: { 
        type: String, 
        required: true
    },
    dataConclusao: { 
        type: String, 
        required: true
    },
    createAt: {
        type: Date,
        default: Date.now
    }
})

//CursoSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('Curso', CursoSchema)