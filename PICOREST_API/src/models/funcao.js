'use strict'
const mongoose = require('mongoose')
// const restful = require('node-restful')
// const mongoose = restful.mongoose

const FuncaoSchema = new mongoose.Schema({
    descricao: { 
        type: String, 
        required: true
    },  
    cursos: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Curso'
    }], 
    createAt: {
        type: Date,
        default: Date.now
    }
})

module.exports = mongoose.model('Funcao',FuncaoSchema)
