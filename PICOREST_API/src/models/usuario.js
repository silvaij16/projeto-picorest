'use strict'
const mongoose = require('mongoose')
//const mongoose = require('../database/banco')
// const restful = require('node-restful')
// const mongoose = restful.mongoose
//const mongoosePaginate = require('mongoose-paginate')

const UsuarioSchema = new mongoose.Schema({
    nome: {
        type: String,
        require: true
    },
    cpf: {
        type: String,
        unique: true,
        require: true
    },
    senha: {
        type: String,
        require: true,
        select: false
    },
    createAt: {
        type: Date,
        default: Date.now
    }
})

//UsuarioSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('Usuario', UsuarioSchema)


//https://pt.stackoverflow.com/questions/249830/rela%C3%A7%C3%A3o-entre-documentos-mongodb
//https://www.codementor.io/olatundegaruba/nodejs-restful-apis-in-10-minutes-q0sgsfhbd