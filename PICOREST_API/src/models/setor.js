'use strict'
const mongoose = require('mongoose')
// const mongoose = require('../database/banco')
// const restful = require('node-restful')
// const mongoose = restful.mongoose
//const mongoosePaginate = require('mongoose-paginate')

const SetorSchema = new mongoose.Schema({
    descricao: { 
        type: String, 
        required: true
    }, 
    createAt: {
        type: Date,
        default: Date.now
    }
})

//SetorSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('Setor', SetorSchema)