'use restrict'
module.exports = function(app){

    //curso
    var cursoController = require('../controllers/cursoController')
    
    app.route('/cursos')
    .get(cursoController.read)
    .post(cursoController.create);
    
    app.route('/cursos/:id')
    .get(cursoController.show)
    .put(cursoController.update)
    .delete(cursoController.delete);

    //Função
    var FuncaoController = require('../controllers/FuncaoController')
    
    app.route('/funcoes')
    .get(FuncaoController.read)
    .post(FuncaoController.create);
    
    app.route('/funcoes/:id')
    .get(FuncaoController.show)
    .put(FuncaoController.update)
    .delete(FuncaoController.delete);

    //Funcionário
    var FuncionarioController = require('../controllers/FuncionarioController')
    
    app.route('/funcionarios')
    .get(FuncionarioController.read)
    .post(FuncionarioController.create);
    
    app.route('/funcionarios/:id')
    .get(FuncionarioController.show)
    .put(FuncionarioController.update)
    .delete(FuncionarioController.delete);

    //setor
    var setorController = require('../controllers/setorController')
    
    app.route('/setores')
    .get(setorController.read)
    .post(setorController.create);
    
    app.route('/setores/:id')
    .get(setorController.show)
    .put(setorController.update)
    .delete(setorController.delete);

    //usuário
    var usuarioController = require('../controllers/usuarioController')
    
    app.route('/usuarios')
    .get(usuarioController.read)
    .post(usuarioController.create);
    
    app.route('/usuarios/:id')
    .get(usuarioController.show)
    .put(usuarioController.update)
    .delete(usuarioController.delete);
     
}

//https://www.codementor.io/olatundegaruba/nodejs-restful-apis-in-10-minutes-q0sgsfhbdapp.route
   

    // app.route('/usuario')
    // .get(() => {console.log("usuario get")})
    // .post(() => {console.log('Criar usuário')})

    // app.route('/setor')
    // .get(() => {console.log("setor get")})