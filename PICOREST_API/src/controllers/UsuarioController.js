'use strict'
const mongoose = require('mongoose')
const Usuario = mongoose.model('Usuario')

module.exports = {
    async read(req, res) {
        try {
            const usuarios = await Usuario.find() 

            return res.json(usuarios)

        } catch (error) {
            res.status(400).send({ error: 'Falha em Localizar usuario' });

        }
    },
    
    async show(req, res) {
        const usuario = await Usuario.findById(req.params.id)

        return res.json(usuario)
    },

    async create(req, res) {
        try {
            const usuario = await Usuario.create(req.body)
            return res.json(usuario)

        } catch (error) {
            res.status(400).send({ error: 'Falha em tentar registrar' });
        }
    },

    async update(req, res) {
        const usuario = await Usuario.findByIdAndUpdate(req.params.id, req.body, {new: true})

        return res.json(usuario)
    },

    async delete(req, res) {
        await Usuario.findByIdAndRemove(req.params.id)

        return res.send("foi-se!")
    }
}