'use strict'
const mongoose = require('mongoose')
const Curso = mongoose.model('Curso')

module.exports = {
    async read(req, res) {
        //const { page = 1 } = req.query //desestruturação E6
        const cursos = await Curso.find() 

        return res.json(cursos)
    },
    
    async show(req, res) {
        const curso = await Curso.findById(req.params.id)

        return res.json(curso)
    },

    async create(req, res) {
        const curso = await Curso.create(req.body)

        return res.json(curso)
    },

    async update(req, res) {
        const curso = await Curso.findByIdAndUpdate(req.params.id, req.body, {new: true})

        return res.json(curso)
    },

    async delete(req, res) {
        await Curso.findByIdAndRemove(req.params.id)

        return res.send("foi-se!")
    }
}