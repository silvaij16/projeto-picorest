'use strict'
const mongoose = require('mongoose')
const Setor = mongoose.model('Setor')

module.exports = {
    async read(req, res) {
        //const { page = 1 } = req.query //desestruturação E6
        const setores = await Setor.find()

        return res.json(setores)
    },
    
    async show(req, res) {
        const setor = await Setor.findById(req.params.id)

        return res.json(setor)
    },

    async create(req, res) {
        const setor = await Setor.create(req.body)

        return res.json(setor)
    },

    async update(req, res) {
        const setor = await Setor.findByIdAndUpdate(req.params.id, req.body, {new: true})

        return res.json(setor)
    },

    async delete(req, res) {
        await Setor.findByIdAndRemove(req.params.id)

        return res.send("foi-se!")
    }
}