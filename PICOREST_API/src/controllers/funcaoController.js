'use strict'
const mongoose = require('mongoose')
const Funcao = mongoose.model('Funcao')
//const Curso = mongoose.model('Curso')

module.exports = {
    async read(req, res) {
        const funcoes = await Funcao.find()

        return res.json(funcoes)
    },
    
    async show(req, res) {
        const funcao = await Funcao.findById(req.params.id)

        return res.json(funcao)
    },

    
    async create(req, res) {
        const funcao = await Funcao.create(req.body)

        return res.json(funcao)
    },

    async update(req, res) {
        const funcao = await Funcao.findByIdAndUpdate(req.params.id, req.body, {new: true})

        return res.json(funcao)
    },

    async delete(req, res) {
        await Funcao.findByIdAndRemove(req.params.id)

        return res.send("foi-se!")
    }
}




// async read(req, res) {
//     const { page = 1 } = req.query //desestruturação E6
//     const funcoes = await Funcao.paginate({}, { page, limit: 30} ) 

//     return res.json(funcoes)
// },


// async create(req, res) {
//     const { descricao, cursos } = req.body

//     const funcao = await Funcao.create({ descricao })

//     await Promise.all(cursos.map(async curso => {
//         const cursoFuncao = new Curso({ ...curso, funcao: funcao._id})
        
//         await cursoFuncao.save()
        
//         funcao.cursos.push(cursoFuncao)
//     }))

//     await funcao.save()
//     return res.json({funcao})
// },