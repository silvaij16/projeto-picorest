'use strict'
const mongoose = require('mongoose')
const Funcionario = mongoose.model('Funcionario')
// const Curso = mongoose.model('Curso')
// const Funcao = mongoose.model('Funcao')
// const Setor = mongoose.model('Setor')

module.exports = {
    async read(req, res) {
        const { page = 1 } = req.query //desestruturação E6
        const funcionarios = await Funcionario.paginate({}, { page, limit: 30} ) 

        return res.json(funcionarios)
    },
    
    async show(req, res) {
        const funcionario = await Funcionario.findById(req.params.id)

        return res.json(funcionario)
    },

    async create(req, res) {
        const funcionario = await Funcionario.create(req.body)

        return res.json(funcionario)
    },

    async update(req, res) {
        const funcionario = await Funcionario.findByIdAndUpdate(req.params.id, req.body, {new: true})

        return res.json(funcionario)
    },

    async delete(req, res) {
        await Funcionario.findByIdAndRemove(req.params.id)

        return res.send("foi-se!")
    }
}