const express = require("express")
const app = express()
const porta = 3046
const mongoose = require('mongoose')
const requireDir = require('require-dir')
const bodyParser = require('body-parser')

requireDir("./src/models")

//instanciando o banco(conexão)
mongoose.Promise = global.Promise
mongoose.connect('mongodb://localhost:27017/3picorestBanco',{ useNewUrlParser: true });

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

//rotas
var routes = require('./src/routes/routes') //importando a rota
routes(app)// registrano a rota. Então podemos usar o app em routes

app.listen(porta)

console.log('rodando')

//https://www.youtube.com/watch?v=BN_8bCfVp88&list=PL85ITvJ7FLoiXVwHXeOsOuVppGbBzo2dp
// C:\Program Files\MongoDB\Server\4.2\bin>mongod --dbpath C:\Users\israelpc\Desktop\data